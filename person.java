import javafx.beans.property.SimpleStringProperty;

public class Person{
	

	private SimpleStringProperty firstName_;
	private SimpleStringProperty lastName_;
	private SimpleStringProperty address_;
	private SimpleStringProperty postNumber_;
	private SimpleStringProperty city_;
	private SimpleStringProperty phoneNumber_;

	
	
	public Person() 
	{
		
		firstName_ =new SimpleStringProperty("");
		lastName_ = new SimpleStringProperty("");
		address_ =new SimpleStringProperty("");
		postNumber_ =new SimpleStringProperty("");
		city_ = new SimpleStringProperty("");;
		phoneNumber_ = new SimpleStringProperty("");
		
	}
	
	public Person(String firstName, String lastName, String address, String postNumber,String city,String phoneNumber )
	{
		
		this.firstName_ = new SimpleStringProperty(firstName);
		this.lastName_ = new SimpleStringProperty(lastName);
		this.address_ =new SimpleStringProperty(address);
		this.postNumber_ =new SimpleStringProperty(postNumber);
		this.city_ = new SimpleStringProperty(city);
		this.phoneNumber_ = new SimpleStringProperty(phoneNumber);
		
		
	}
	
	public String getFirstName_()
	{
		return firstName_.get();
	}
	public String getLastName_()
	{
		return lastName_.get();
	}
	public String getAddress_()
	{
		return address_.get();
	}
	public String getPostNumber_()
	{
		return postNumber_.get();
	}
	public String getCity_()
	{
		return city_.get();
	}
	public String getPhoneNumber_()
	{
		return phoneNumber_.get();
	}

	public void setFirstName_(SimpleStringProperty firstName_) {
		this.firstName_ = firstName_;
	}

	public void setLastName_(SimpleStringProperty lastName_) {
		this.lastName_ = lastName_;
	}

	public void setAddress_(SimpleStringProperty address_) {
		this.address_ = address_;
	}

	public void setPostNumber_(SimpleStringProperty postNumber_) {
		this.postNumber_ = postNumber_;
	}

	public void setCity_(SimpleStringProperty city_) {
		this.city_ = city_;
	}

	public void setPhoneNumber_(SimpleStringProperty phoneNumber_) {
		this.phoneNumber_ = phoneNumber_;
	}


	
}
