import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {
	
	private Connection connect = null;/*
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;*/

	  public Connection getConnection() throws SQLException, ClassNotFoundException 
	  {
		  
		// This will load the MySQL driver, each DB has its own driver
		  Class.forName("com.mysql.jdbc.Driver");
	      // Return the DBconnection object
		  
		  connect = DriverManager.getConnection("jdbc:mysql://192.168.1.95:3306/phonebook?"
	              + "user=testuser&password=testaaja");
		return connect;
	  }
	  
	  
	  
	  /*
	  public void writePersonToDB(Person person) throws Exception
	  {
		  try {
	      // PreparedStatements can use variables and are more efficient
	      preparedStatement = connect
	          .prepareStatement("insert into Persons values (?,?,?,?,?,?,?)");
	      // Parameters start with 
	      preparedStatement.setInt(1, 0);
	      preparedStatement.setString(2, person.getFName());
	      preparedStatement.setString(3, person.getLName());
	      preparedStatement.setString(4, person.getCity());
	      preparedStatement.setString(5,person.getPostnumber() );
	      preparedStatement.setString(6, person.getPhonenumber());
	      preparedStatement.setString(7, person.getAddress());
	      preparedStatement.executeUpdate();
		  }
		  catch(Exception e)
		  {
			  throw e;
		  }
		  
		  finally
		  {
			  close();
		  }
		  
	  }
	  
	  public ObservableList<Person> printPersonData() throws Exception
	  {
		  ObservableList<Person> result = FXCollections.observableArrayList();
		  try
		  {
			  
			   preparedStatement = connect.prepareStatement("SELECT * from Persons");
				      resultSet = preparedStatement.executeQuery();
				      
				    //  writeResultSet(resultSet);
	
		  }
		  catch(Exception e)
		  {
			 
			  throw e;
			  
		  }
		  finally
		  {
			  
			  while (resultSet.next()) 
			  {
			    	
			   //person(String etunimi, String sukunimi, String kotiosoite, String postinumero,String postitoimipaikka,String puhelinnumero )
			    Person resultPerson = new Person(
			    		resultSet.getString("firstName"),
			    		resultSet.getString("lastName"),
			    		resultSet.getString("address"),
			    		resultSet.getString("postNumber"),
			    		resultSet.getString("city"),
			    		resultSet.getString("phoneNumber")
			    		);
			    	result.add(resultPerson);
			  }
			  
			  close();
			  
			  
		  }
		  return result;
	  }
	  


		/*  private List<person> writeResultSet(ResultSet resultSet) throws SQLException 
		  {
			
			  List<person> result = new ArrayList<person>();
		    // ResultSet is initially before the first data set
		    while (resultSet.next()) 
		    {
		    	
		   //person(String etunimi, String sukunimi, String kotiosoite, String postinumero,String postitoimipaikka,String puhelinnumero )
		    person resultPerson = new person(
		    		resultSet.getString("firstName"),
		    		resultSet.getString("lastName"),
		    		resultSet.getString("address"),
		    		resultSet.getString("postNumber"),
		    		resultSet.getString("city"),
		    		resultSet.getString("phoneNumber")
		    		);
		    	result.add(resultPerson);
		    	
		    }
			return result;
		  }
		  */

		  // You need to close the resultSet
	  /*
		  private void close() 
		  {
		    try 
		    {
		      if (resultSet != null) 
		      {
		        resultSet.close();
		      }

		      if (statement != null) 
		      {
		        statement.close();
		      }

		      if (connect != null) 
		      {
		        connect.close();
		      }
		    } 
		    
		    catch (Exception e) 
		    {

		    }
		  }
	
	*/
}
