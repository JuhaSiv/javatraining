
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class mainApplication extends Application {

	private TableView<Person> table = new TableView<Person>();
	
	
	ResultSet rs;
	PreparedStatement ps;
	DBConnection db = new DBConnection();
	

	private ObservableList<Person> data;
//	ObservableList<Person> list = FXCollections.observableArrayList();
	
	Label firstNameLabel;
	TextField firstNameField;

	Label lastNameLabel;
	TextField lastNameField;

	Label phoneNumberLabel;
	TextField phoneNumberField;

	Label addressLabel;
	TextField addressField;

	Label cityLabel;
	TextField cityField;

	Label postNumberLabel;
	TextField postNumberField;

	Text notificationMessage;

	Button savePerson;

	public static void main(String[] args) throws Exception 
	{

		launch(args);
	
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		
	//	connection getList = new connection();
		
		//list = getList.printPersonData();

		primaryStage.setTitle("Phonebook");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Scene scene = new Scene(grid, 1300, 350);
		primaryStage.setScene(scene);

		Text scenetitle = new Text("Add a person to phonebook");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);

		// Set labels
		firstNameLabel = new Label("Firstname:");// 1
		lastNameLabel = new Label("Lastname:");// 2
		phoneNumberLabel = new Label("Phonenumber:");// 3
		addressLabel = new Label("Address:");// 4
		postNumberLabel = new Label("Postnumber:");// 5
		cityLabel = new Label("City:");// 6
		notificationMessage = new Text();

		// Add lables to grid
		grid.add(firstNameLabel, 0, 1);
		grid.add(lastNameLabel, 0, 2);
		grid.add(phoneNumberLabel, 0, 3);
		grid.add(addressLabel, 0, 4);
		grid.add(postNumberLabel, 0, 5);
		grid.add(cityLabel, 0, 6);

		// Set textfields

		firstNameField = new TextField();// 1
		lastNameField = new TextField();// 2
		phoneNumberField = new TextField();// 3
		addressField = new TextField();// 4
		postNumberField = new TextField();// 5
		cityField = new TextField();// 6

		// Add textfields to grid
		grid.add(firstNameField, 1, 1);
		grid.add(lastNameField, 1, 2);
		grid.add(phoneNumberField, 1, 3);
		grid.add(addressField, 1, 4);
		grid.add(postNumberField, 1, 5);
		grid.add(cityField, 1, 6);
		grid.add(notificationMessage, 1, 8);//placed after button

		savePerson = new Button();
		savePerson.setText("Save");

		savePerson.setOnAction(new EventHandler<ActionEvent>() 
		{

			@Override
			public void handle(ActionEvent event) 
			{

				Person person = new Person(firstNameField.getText(), 
						lastNameField.getText(), 
						addressField.getText(),
						postNumberField.getText(), 
						cityField.getText(), 
						phoneNumberField.getText());
				
				data.add(person);
				try 
				{
					//connect to database
					Connection conn = db.getConnection();
					PreparedStatement pr;
					
					//SQL query string
					String sql = "insert into Persons values (?,?,?,?,?,?,?)";
					
					
					pr =  conn.prepareStatement(sql);
					//Parameters for preparedStatement
				      pr.setInt(1, 0);
				      pr.setString(2, person.getFirstName_());
				      pr.setString(3, person.getLastName_());
				      pr.setString(4, person.getCity_());
				      pr.setString(5,person.getPostNumber_() );
				      pr.setString(6, person.getPhoneNumber_());
				      pr.setString(7, person.getAddress_());
				      pr.executeUpdate();
					
				} 
				
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				finally 
				{
					firstNameField.clear();
					lastNameField.clear();
					addressField.clear();
					postNumberField.clear();
					cityField.clear();
					phoneNumberField.clear();

					notificationMessage.setText("Person added to database");
				}

			}
		});

		grid.add(savePerson, 0, 7);

		

		//Set columns for the table.
		TableColumn firstNameCol = new TableColumn("First name");
		firstNameCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("firstName_")
			);
		
		TableColumn lastNameCol = new TableColumn("Last name");
		lastNameCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("lastName_")
			);
		
		TableColumn addressCol = new TableColumn("Address");
		addressCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("address_")
			);
		
		TableColumn postNumberCol = new TableColumn("Postnumber");
		postNumberCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("postNumber_")
			);
		
		TableColumn phoneNumberCol = new TableColumn("Phonenumber");
		phoneNumberCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("phoneNumber_")
			);

		TableColumn cityCol = new TableColumn("City");
		cityCol.setCellValueFactory(
			    new PropertyValueFactory<Person,String>("city_")
			);
		
		//populate the table OR NOT FUCK THIS SHIT
		table.setEditable(true);
		   try
		   {
				getTableData();
			
		   }
		    catch(ClassNotFoundException ce)
		   {
		        throw ce;
		    }
		    catch(SQLException ce)
		   {
		        throw ce;
		    }
	
	
	
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.getColumns().addAll(firstNameCol, lastNameCol, addressCol, postNumberCol, phoneNumberCol, cityCol);
		grid.add(table, 2, 0);

		grid.setRowSpan(table, 7);
		
		primaryStage.show();

	}
	
	//function to get the data for the table.
	public void getTableData() throws Exception
	{
			  data = FXCollections.observableArrayList();
			  Connection conn = db.getConnection();
			  try
			  {
				
				  String SQL = "SELECT * from Persons";
				  ps = conn.prepareStatement(SQL);
				  rs = ps.executeQuery();
				  while (rs.next()) 
				  {
					  System.out.println(rs.getString("firstName"));
				  Person resultPerson = new Person(
				    		rs.getString("firstName"),
				    		rs.getString("lastName"),
				    		rs.getString("address"),
				    		rs.getString("postNumber"),
				    		rs.getString("city"),
				    		rs.getString("phoneNumber")
				    		);
				    	data.add(resultPerson);
				  }
				table.setItems(data);
			  }
			
			  catch(Exception e)
			  {
				   
			  }
			  
			  
	  
	}

}
